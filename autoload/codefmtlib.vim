" Copyright 2014 Google Inc. All rights reserved.
"
" Licensed under the Apache License, Version 2.0 (the "License");
" you may not use this file except in compliance with the License.
" You may obtain a copy of the License at
"
"     http://www.apache.org/licenses/LICENSE-2.0
"
" Unless required by applicable law or agreed to in writing, software
" distributed under the License is distributed on an "AS IS" BASIS,
" WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
" See the License for the specific language governing permissions and
" limitations under the License.

""
" @section Introduction, intro
" @library
" CODEFMTLIB IS DEPRECATED
"
" This library was previously used as the interface between the |codefmt|
" plugin and other plugins that register formatters for codefmt. codefmt now
" uses Maktaba for formatter registration, and so codefmtlib is no longer
" useful.


""
" @dict Formatter
" Interface for applying formatting to lines of code.
" Defines these fields:
"   * name (string): The formatter name that will be exposed to users.
"   * setup_instructions (string, optional): A string explaining to users how to
"     make the plugin available if not already available.
" and these functions:
"   * IsAvailable() -> boolean: Whether the formatter is fully functional with
"     all dependencies available. Returns 0 only if setup_instructions have not
"     been followed.
"   * AppliesToBuffer() -> boolean: Whether the current buffer is of a type
"     normally formatted by this formatter. Normally based on 'filetype', but
"     could depend on buffer name or other properties.
" and should implement at least one of the following functions:
"   * Format(): Formats the current buffer directly.
"   * FormatRange({startline}, {endline}): Formats the current buffer, focusing
"     on the range of lines from {startline} to {endline}.
"   * FormatRanges({ranges}): Formats the current buffer, focusing on the given
"     ranges of lines. Each range should be a 2-item list of
"     [startline,endline].
" Formatters should implement the most specific format method that is supported.

""
" Registers {formatter} as a supported formatter. See @dict(Formatter) for the
" API {formatter} must implement.
"
" This method is deprecated. Callers should replace calls to this with the
" following:
" >
"   call maktaba#extension#GetRegistry('codefmt').AddExtension(formatter)
" <
"
" @throws WrongType if {formatter} is not a dict.
function! codefmtlib#AddFormatter(formatter) abort
  call maktaba#extension#GetRegistry('codefmt').AddExtension(a:formatter)
endfunction

""
" @private
" Obsolete. Provided here only to provide a sensible error message.
function! codefmtlib#AddDefaultFormatter(formatter) abort
  throw maktaba#error#NotImplemented('No longer available.')
endfunction

""
" @private
" Obsolete. Provided here only to provide a sensible error message.
function! codefmtlib#GetFormatters() abort
  throw maktaba#error#NotImplemented('No longer available.')
endfunction
