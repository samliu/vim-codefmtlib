# DEPRECATED

codefmtlib was previously used as the interface between the
[codefmt](https://github.com/google/vim-codefmt) plugin and other plugins that
register formatters for codefmt.  codefmt now uses Maktaba for formatter
registration, and so codefmtlib is no longer useful.
